#!/bin/bash
# HyperCaster content API metadata update tool.
#
# Purpose: update content metadata using the content API
#
# Using a dedicated single record flat file, written by a database
# The variables are parsed into a curl data parameter list
#
# Looks for content by filename
# If not found, creates new content placeholder metadata
#
# If found, checks for a file attached or not (unattached)
#
# If unattached, edits content placeholder metadata
# if attached, edits content metadata (a few variables change names)
#
#

# testing?
TESTING=0


# HyperCater Server address (IP or domain)
api_address=your.server.address

# log file
log_file=$HOME/hypercaster_edit.log

# content file import date and time used for the_defaults_datatable
# import_datetime must be now or in the future
default_import_datetime=$(date "+%Y-%m-%d %H:%M:%S")

# content file delete date and time used for the_defaults_datatable
#   example: now +2 years  => -v+2y   now + 90 days  => -v+90d
default_delete_datetime=$(date -v+2y "+%Y-%m-%d %H:%M:%S")

# curl config file, used to build the curl command for execution
curl_config_datafile="/WebMacro/hypercaster_content_record_update.curlconfig"


## where's the data?
# format:
#   each row is a variable and value pair, delineated by the first =
#      eg. variable=value continues to end of the line.
#
#   expected_filename is required and is used to determine if a record exists
#   the filename is tested with /content_metadata_by_filename/$filename
#      filename=expected_filename.ts
#
#   see the_defaults section for format
#
data_file="/WebMacro/hypercaster_content_record_update.datafile"


# the_defaults
# HyperCast API variables may be assigned default values
# if not present in the data_file, these will be used
#
# this also serves as an example data_file
#
# depending on your environment, you may want to keep the API key here
#
# import_datetime is required
#  (# comment will skip a row)
the_defaults_datatable=$(cat <<BEGIN_AND_END_OF_LIST
#:filename=some_test_file.ts
:api_key=0123456789asdfghjklStatus 
:prog_title=
:prog_code=
:epis_title=
:epis_code=
:description=
:delete_datetime=$default_delete_datetime
:import_datetime=$default_import_datetime
:offset=00:00:00
:runtime=
:location=
:producer=
:prod_source=
:sched_use=
:op_notes=
:con_tags=
:category1=
#:category2=Education
#:category3=Information
#:categoryX=Skipping
BEGIN_AND_END_OF_LIST
)

### API variable to datafile variable equivalence table
#
# row format:
#   :datafile_variable_name=API_variable_name optional_subroutine
#
#     eg. expected_duration=runtime hms_to_sec
#
api_to_data=$(cat <<BEGIN_AND_END_OF_LIST
:expected_filename=filename
:api_key=api_key
:program=prog_title
:program_code=prog_code
:episode=epis_title
:episode_code=epis_code
:description=description
:delete_datetime=delete_datetime
:import_datetime=import_datetime
:offset=offset hms_to_sec
:expected_duration=runtime hms_to_sec
:duration=runtime hms_to_sec
:location=location
:contributor=producer
:custom_metadata[Producer Source]=prod_source
:custom_metadata[Schedule Use]=sched_use
:custom_metadata[Operator Notes]=op_notes
:custom_metadata[Content Tags]=con_tags
:categories[]=category1
:categories[]=category2
:categories[]=category3
BEGIN_AND_END_OF_LIST
)

### new record API variable list
#
# list of variable names to send to REST API, for new content
#  (# comment will skip a row)
new_record=$(cat <<BEGIN_AND_END_OF_LIST
expected_filename
api_key
program
program_code
episode
episode_code
description
delete_datetime
import_datetime
expected_duration
#location
contributor
custom_metadata[Producer Source]
custom_metadata[Schedule Use]
custom_metadata[Operator Notes]
custom_metadata[Content Tags]
categories[]
BEGIN_AND_END_OF_LIST
)

### edit record, unattached 
#
# list of variable names to send to REST API, edit unattached content
#  (# comment will skip a row)
edit_record_unattached=$(cat <<BEGIN_AND_END_OF_LIST
expected_filename
api_key
program
program_code
episode
episode_code
description
delete_datetime
import_datetime
expected_duration
#location
contributor
custom_metadata[Producer Source]
custom_metadata[Schedule Use]
custom_metadata[Operator Notes]
custom_metadata[Content Tags]
categories[]
BEGIN_AND_END_OF_LIST
)

### edit record, attached
#
# list of variable names to send to REST API, edit attached content
#  (# comment will skip a row)
edit_record_attached=$(cat <<BEGIN_AND_END_OF_LIST
api_key
program
program_code
episode
episode_code
description
delete_datetime
offset
duration
#location
contributor
custom_metadata[Producer Source]
custom_metadata[Schedule Use]
custom_metadata[Operator Notes]
custom_metadata[Content Tags]
categories[]
BEGIN_AND_END_OF_LIST
)


#### [ -z ${var+x} ] && echo "var is not set"

#### a few functions
#

# simple logging tool
function log_event ()
{
	# write the date and time with message to a log file
	DATE_TIME=$(date +'%Y-%m-%d %H:%M:%S')
	
	# date-time [proc_id script] message  to  logfile
	echo "$DATE_TIME [$$] $1" >> "$log_file"
}


# read line wrap for log_event
function log_event_read ()
{
	while read this_line
	do
		log_event "$this_line"
	done
}


# convert hh:mm:ss to seconds
function hms_to_sec ()
{
  # hh * 3600 + mm * 60 + ss
  echo "$1" | sed 's/:/ * 3600 + /;s/:/ * 60 + /' | bc
}


# convert string to url escaped string, with exception for []
function url_encode_spec ()
{
  # URL encode with perl
  url_enc=$(perl -MURI::Escape -e 'print uri_escape($ARGV[0]);' "$1")
  # restore [ and ]
  echo "$url_enc" | sed 's/%5B/\[/g;s/%5D/]/g'
}


# make curl data from variable/value from the_datafile_datatable/the_defaults, per API var name
function data_from_api_var ()
{
  # 1: string: api variable name to convert to data_file var name, and determine value
  # 2: string: (optional) flag to return only the value
  #     any string = return the bald value, no --data-urlencode="" wrapper
  #
  # output: curl data parameter
  api_var_name="$1"
  value_only="$2"
  
  
  # escaped API var name
  urlenc_var_name=$(url_encode_spec "$api_var_name")
  [ $TESTING -gt 0 ] && log_event "TEST(urlenc_var_name): $urlenc_var_name"


  # variable list (handle multiple entries for categories)
  datafile_var_name_proc=$(echo "$api_to_data" | fgrep ":${api_var_name}=" | cut -d '=' -f 2)
  [ $TESTING -gt 0 ] && log_event "TEST(datafile_var_name_proc): $datafile_var_name_proc"
  
  [[ "$datafile_var_name_proc" == "" ]] && {
    message="ERROR: variable doesn't match! (api_to_data) typo=$api_var_name"
    log_event "$message"
    echo "$message"
    exit -22
  }
  
  
  # parse the_datafile_datatable and the_defaults for api_var_name and value
  is_data=
  
  # look in datafile first...
  doing_what="datafile"
  this_datatable="$the_datafile_datatable"
  
  is_data=$(do_this_var)
  
  # if not found in datafile, we look in defaults
  [[ "$is_data" == "" ]] && {
    glue=
    doing_what="defaults"
    this_datatable="$the_defaults_datatable"
    
    is_data=$(do_this_var)
  }
  
  # return value (when needed)
  echo "$is_data"
}


# process var list subroutine for data_from_api_var
function do_this_var ()
{  
  # process matched var list
  while read this_df_var_name_proc
  do
    [ $TESTING -gt 0 ] && log_event "TEST(this_df_var_name_proc): $this_df_var_name_proc"
    
    # get just the var name if we've a processor indicated
    this_var_name=$(echo "$this_df_var_name_proc" | cut -d ' ' -f 1)
    [ $TESTING -gt 0 ] && log_event "TEST(this_var_name): $this_var_name"
    
    # get processor if indicated
    processor=
    is_space=$(echo "$this_df_var_name_proc" | grep -o ' ')
    [[ "$is_space" != "" ]] && {
      processor=$(echo "$this_df_var_name_proc" | cut -d ' ' -f 2)
    }
    [ $TESTING -gt 0 ] && log_event "TEST(processor): $processor"
  
    # is this_var_name defined in datatable 
    the_row=$(echo "$this_datatable" | fgrep ":${this_var_name}=")
    [ $TESTING -gt 0 ] && log_event "TEST(the_row): $the_row"
    
    # we have data...
    [[ "$the_row" != "" ]] && {      
      # skip commented rows 
      [[ "${the_row:0:1}" == "#" ]] && {
        log_event "---SKIPPING COMMMENT($doing_what): $the_row"
        continue
      }
      
      # data match is good, accumulate the response
      this_value=${the_row#*=}
      
      # processor?
      [[ "$processor" != "" ]] && {
        this_value=$($processor "$this_value")
        [ $TESTING -gt 0 ] && log_event "TEST(processed value): $this_value"
      }
      
      # escape for the curl config file: \ => \\  and  " => \"
      this_value_esc_quot=$(echo "$this_value" | sed 's/\\/\\\\/g; s/"/\\"/g')
      
      [[ "$value_only" != "" ]] && {
        # return the variable value
        echo "$this_value"
      }
      
      [[ "$value_only" == "" ]] && {
        # push the curl data parameter to the config file
        echo "--data-urlencode \"${urlenc_var_name}=${this_value_esc_quot}\"" >> "$curl_config_datafile"
        echo "VALUE APPENDED"
      }
      
    }
  done < <(echo "$datafile_var_name_proc")
}



#################
#### BEGIN
log_event_read <<HERE

----STARTING----  $(date)
HERE
[ $TESTING -eq 1 ] && {
  [ $TESTING -gt 0 ] && log_event "TESTING = true"
}


# get the data...
[ ! -f "$data_file" ] && {
  # the data_file doesn't exist
  message="ERROR: data_file missing: $data_file"
  log_event "$message"
  echo "$message"
  exit -22
}
the_datafile_datatable=$(cat "$data_file")



## check content filename, is there a content record
# get filename from data/defaults
content_filename=$(data_from_api_var expected_filename value)
[[ "$content_filename" == "" ]] && {
  message="ERROR: content_filename is empty!"
  log_event "$message"
  echo "$message"
  exit -22
}

log_event "---CONTENT FILE: $content_filename"


# metadata request (decide if new or edit record, and if attached)
this_curl="http://$api_address/content_metadata_by_filename/$content_filename"
content_metadata=$(curl -s -i "$this_curl")

log_event_read <<HERE

---CONTENT METADATA RESPONSE:

$content_metadata
HERE


:<<SKIPPING
new service, no Status header.
skipping this check should be okay.
# response? (looking for the "Status:" header)
lookfor_stat=$(echo "$content_metadata" | grep -E '^Status: ')
md_request_status=${lookfor_stat#*Status: }

# no response?
[[ "$md_request_status" == "" ]] && {
  message="ERROR: Getting no response with: $this_curl"
  log_event "$message"
  echo "$message"
  exit -22
}
log_event "---request status: $md_request_status"
SKIPPING

# request error?
lookfor_err=$(echo "$content_metadata" | grep "<error>")
lookfor_err=${lookfor_err#*<error>}
md_request_error_string=${lookfor_err%</error>}


# soft error
[[ "$md_request_error_string" != "" ]] && {
  log_event "---error: $md_request_error_string"
}


# no error? get ID
content_ID=
[[ "$md_request_error_string" == "" ]] && {
  ## parse ID
  ## <id type='integer'>123</id>
	get_ID=$(grep "<id type=.integer.>" <<<"$content_metadata")
	get_ID=${get_ID#*<id*>}
	content_ID=${get_ID%%</id>*}
	
	[[ "$content_ID" == "" ]] && {
	  # we failed to parse the ID
	  log_event "ERROR: ID parse fail"
	  exit -22
	}
	
	log_event_read <<HERE

---CONTENT ID: $content_ID
HERE
}

# get attached status
is_attached=
[[ "$content_ID" != "" ]] && {
  ## parse attached
	## <attached type='boolean'>false</attached>
	## <attached type="NilClass">true</attached>
	get_attached=$(grep "<attached " <<<"$content_metadata")
	get_attached=${get_attached#*<attached*>}
	is_attached=${get_attached%%</attached>*}
	log_event "---ATTACHED: $is_attached"
}



###############################
###  what form are we building?

what_form=

# new record...
[[ "$content_ID" == "" ]] && {
  doing_what="new record"
  curl_end="http://$api_address/content_api/new"
  what_form="$new_record"
}

# edit record...
[[ "$content_ID" != "" ]] && {
  # unattached
  [[ "$is_attached" == "false" ]] && {
    doing_what="edit record unattached"
    curl_end="http://$api_address/content_api/$content_ID/edit"
    what_form="$edit_record_unattached"
  }
  
  # attached
  [[ "$is_attached" == "true" ]] && {
    doing_what="edit record attached"
    curl_end="http://$api_address/content_api/$content_ID/edit"
    what_form="$edit_record_attached"
  }
}

log_event "DOING: $doing_what"

# do we have a form?
[[ "$what_form" == "" ]] && {
  message='ERROR: form selection conditions not met.'
  log_event "$message"
  echo "$message"
  exit -22
}


## build curl command
# start with required parameters
echo '# START curl form output: '$(date) > "$curl_config_datafile"


# include the headers
echo "--include" >> "$curl_config_datafile"

# don't show progress or error messages
echo "--silent" >> "$curl_config_datafile"

# doing a GET form
echo "--get" >> "$curl_config_datafile"

# accumulate parameters
while read this_var
do
  # skip comments
  [[ "${this_var:0:1}" == "#" ]] && {
    log_event "---SKIPPING COMMMENT($doing_what): $this_var"
    continue
  }
  
  is_appended=$(data_from_api_var "$this_var")
  [ $TESTING -gt 0 ] && log_event "TEST($doing_what: is_appended): $is_appended"
  
done < <(echo "$what_form")

## end with the URL and a comment
#  echo "--url http://www-p.metroeast.org/_site/form_what.php" >> "$curl_config_datafile"
echo "--url $curl_end" >> "$curl_config_datafile"
echo '# END curl form output: '$(date) >> "$curl_config_datafile"


# test? only echo the curl command, and exit
[ $TESTING -eq 1 ] && {
  log_event "TESTING: END:::"
  echo "curl --config '$curl_config_datafile'"
  echo
  cat "$curl_config_datafile"
  exit -13
}


######################
### send the form
api_response=$(curl --config "$curl_config_datafile")

log_event_read <<HERE

---API RESPONSE:

$api_response
HERE


:<<SKIPPING
new service, no Status header.
skipping this check should be okay.
# response? (looking for the "Status:" header)
lookfor_stat=$(echo "$api_response" | grep -E '^Status: ')
md_request_status=${lookfor_stat#*Status: }

# no response?
[[ "$md_request_status" == "" ]] && {
  message="ERROR: Getting no response with: curl --config '$curl_config_datafile'"
  log_event "$message"
  echo "$message"
  exit -22
}
log_event "---request status: $md_request_status"
SKIPPING


# get request status
is_success=

# looking for true or false at on a line by itself
get_stat_false=$(echo "$api_response" | grep -E '^false$')
get_stat_true=$(echo "$api_response" | grep -E '^true$')

# neither?
[[ "$get_stat_false$get_stat_true" == "" ]] && {
  message="ERROR: Somehow we have neither true nor false, in the response from the server."
  log_event "$message"
  echo "$message"
  exit -22
}


# so are we done? (request success?)
[[ "$get_stat_false" != "" ]] && is_success=false
[[ "$get_stat_true" != "" ]] && is_success=true
log_event "---SUCCESS?: $is_success"

# on failure...
[[ "$get_stat_false" != "" ]] && {
  # alert
  message='ERROR: API request failed.  Hmmm...'
  log_event "$message"
  echo "$message"
  exit -86

:<<NOTE
  # parse request error?
  lookfor_err=$(echo "$api_response" | grep "<error>")
  lookfor_err=${lookfor_err#*<error>}
  api_response_error_string=${lookfor_err%</error>}
NOTE

}


# success
exit 0
