# README #

We needed a connector between our database and our automation.  The database is a custom set of scripts, and the automation is a TelVue HyperCaster.

Initially the fields we wanted to edit were not available, so we chose to simulate the login and form submission process.

Later, the API was extended to support the fields of interest.
We rewrote the basic submission script and then completely rewrote this tool to support more flexibility with the variables being processed.



### What is this repository for? ###

potentially useful for other integrations.
testing.
practice.



### How do I get set up? ###

* you might want to adjust the path to the log and the datafile.
* you'll want to make the datafile or copy "hypercaster_content_record_update.datafile" from the repo.
* an example of the datafile format resides in the _here_ document use to assign the_defaults_datatable variable.
* requires a HyperCaster server/service to get anything useful to happen.
* you might want to adjust the date and time variables for import and delete — $default_import_datetime and $default_delete_datetime respectively — to fit your workflow.


### Required variables: ###

* you may want to keep the appropriate API key in the_defaults_datatable variable inside hypercaster_content_udpate.sh, or it can be supplied in the datafile.
* the filename is used to locate a current record to edit; confirming that a record exists.
* if no record is found, a new content (placeholder) record is generated.


### The general usage/workflow is: ###

* Your database writes the datafile.
* hypercaster_content_udpate.sh is executed.
* Depending on the wrapper used to call the shell script, any error messages can be displayed to the user.  (Errors are sent to stdout and the log.)



### Who do I talk to? ###

* metroeast or elkram
